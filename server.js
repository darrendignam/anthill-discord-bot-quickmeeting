//DISCORD BOT STUFF
const Discord = require('discord.js');
require('dotenv').config();
let mongoose = require('mongoose');
let Words = require('./models/word');

mongoose.connect(process.env.MONGO_CONN_STRING);

const client = new Discord.Client();

client.login(process.env.DISCORD_BOT_TOKEN);


client.once('ready', () => {
	console.log('Bot Ready!');
});

const discord_config = { "prefix" : "!" };

client.on("message", async message => {
    // This event will run on every single message received, from any channel or DM.
    
    // It's good practice to ignore other bots. This also makes your bot ignore itself
    // and not get into a spam loop (we call that "botception").
    if(message.author.bot) return;
    
    // Also good practice to ignore any message that does not start with our prefix, 
    // which is set in the configuration file.
    if(!message.content.startsWith(discord_config.prefix)) return;
    
    // Here we separate our "command" name, and our "arguments" for the command. 
    // e.g. if we have the message "+say Is this the real life?" , we'll get the following:
    // command = say
    // args = ["Is", "this", "the", "real", "life?"]
    const args = message.content.slice(discord_config.prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();
    
    // Let's go with a few common example commands! Feel free to delete or change those.
    
    if(command === "ping") {
      // Calculates ping between sending a message and editing it, giving a nice round-trip latency.
      // The second ping is an average latency between the bot and the websocket server (one-way, not round-trip)
      const m = await message.channel.send("Ping?");
      m.edit(`Pong! Latency is ${m.createdTimestamp - message.createdTimestamp}ms. API Latency is ${Math.round(client.ws.ping)}ms`);
    }
    
    if(command === "quickmeeting" || command === "qm" || command === "meeting"){
      // message.channel.send("Meeting Room Created here:");
      // message.channel.send("www.theanthill.io");

      Words.aggregate([ {$match : {}}, {$sample : {size : 4}} ], (err, words)=>{
        if(err){
          message.channel.send("ERROR!");
          message.channel.send(err);
        }else{
          message.channel.send("Meeting space created here:");
          message.channel.send('https://meet.jit.si/'+words[0].word +'-'+words[1].word+'-'+words[2].word+'-'+words[3].word);
          message.channel.send("This room will remain open while ants are using it. When the last person leaves the room, it will everntually be recycled. This room is provided by jit.si");
        }
      });
    }
   

  });