### Discord Quick Meeting Bot

This is a very simple bot. It uses four random words to create a unique meeting room ID - that is then used with the great jit.si video conferencing service to quickly spin up a place for collaboration!

This bot requires a dictionary - currently this is provided by a very simple MongoDB collection of words. But this could be an array of words in a file to make it even simpler!!!